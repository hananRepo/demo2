package com.example.demo.controller;


import com.example.demo.basic.Luas;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/bangunruang")
public class BangunRuangController {

    @RequestMapping(value = {"/",""},method = RequestMethod.GET)
    public String index(){
        return "bangunruang/index";
    }

    @RequestMapping(value = {"/segitiga",""}, method = RequestMethod.GET)
    public String segitiga(){
        return "bangunruang/segitiga";
    }

    @RequestMapping(value = {"/luas/{param}"}, method = RequestMethod.GET)
    public String luas(Model model, @PathVariable("param") String param1){
        model.addAttribute("luas",new Luas());
        model.addAttribute("x",param1);
        return "bangunruang/luas";
    }

    @RequestMapping(value = {"/luas"}, method = RequestMethod.POST)
    public String luasHasil(@ModelAttribute Luas luas){
        return "bangunruang/luas-hasil";
    }


}
