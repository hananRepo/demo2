package com.example.demo.controller;


import com.example.demo.model.Book;
import com.example.demo.repositories.BookRepository;
import com.example.demo.services.BookService;
import com.sun.org.apache.xpath.internal.operations.Mod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import com.example.demo.services.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/book")
public class BookController {

    @Autowired
    BookRepository bookRepository ;

    @RequestMapping(value = {"","/"})
    public String listBook(Model model){
        model.addAttribute("Books",bookRepository.findAll());
        return "books/list";
    }

    @GetMapping("/insert")
    public String goToInput(Model model){
        model.addAttribute("Books", new Book() );
        return "books/insert";
    }

    @PostMapping("/addBook")
    public String addUser(@Valid Book book, BindingResult result, Model model) {

        bookRepository.save(book);
        model.addAttribute("Books", bookRepository.findAll());
        return "redirect:/book";
    }

    @GetMapping("/edit/{id}")
    public String editBook(@PathVariable("id") Integer id, Model model ){
        Book book = bookRepository.findById(id).orElseThrow(()-> new IllegalArgumentException("Invalid Book ID:"+ id));
        model.addAttribute("Books",book);
        return "books/edit";
    }

    @PostMapping("/update/{id}")
    public String updateBook(@PathVariable("id") Integer id, @Valid Book book,BindingResult result, Model model) {
        if (result.hasErrors()) {
            book.setId(id);
            return "books/edit";
        }

        bookRepository.save(book);
        model.addAttribute("Books", bookRepository.findAll());
        return "redirect:/book";
    }

    @GetMapping("/delete/{id}")
    public String deleteBook(@PathVariable("id") Integer id, Model model) {
        Book book = bookRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
        bookRepository.delete(book);
        model.addAttribute("Books", bookRepository.findAll());
        return "redirect:/book";
    }
}
