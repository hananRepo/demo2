package com.example.demo.basic;

public class Luas {

    public  Integer panjang;
    public  Integer lebar;

    public Luas(){

    }

    public  Luas (Integer p, Integer l){
        this.panjang = p;
        this.lebar = l;
    }

    public Integer HitungLuas(){
        Integer hasil = panjang * lebar;
        return hasil;
    }

    public void setLebar(Integer lebar) {
        this.lebar = lebar;
    }

    public void setPanjang(Integer panjang) {
        this.panjang = panjang;
    }

    public Integer getLebar() {
        return lebar;
    }

    public Integer getPanjang() {
        return panjang;
    }
}
