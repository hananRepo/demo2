package com.example.demo.basic;

import java.lang.reflect.Array;

public class Bilangan {

    public Bilangan(){

    }

    public void bilGanjil(Integer x){

        for (Integer i=1; i<=x;i=i+2){
            System.out.println(i);
        }

    }

    public void bilGenap(Integer x){

        for (Integer i=2; i<=x;i=i+2){
            System.out.println(i);
        }

    }

    public String tebilang(Integer x){
        String nilai = "";
        String bil[] = {"","Satu","Dua","Tiga","Empat","Lima","Enam","Tujuh","Delapan","Sembilan","Sepuluh","Sebelas"};
        if (x < 12){
            nilai = bil[x];
        } else if (x<20){
            nilai = tebilang(x-10) +" Belas";
        } else if (x<100){
            nilai = tebilang(x/10) + " Puluh " + tebilang(x%10);
        } else if (x<200){
            nilai = "Seratus " + tebilang(x - 100);
        } else if (x<1000){
            nilai = tebilang(x/100) + " Ratus " + tebilang(x%100);
        } else if (x<2000){
            nilai = "Seribu " + tebilang(x-1000);
        } else if (x<1000000){
            nilai = tebilang(x/1000) + " Ribu " + tebilang(x%1000);
        } else if (x<1000000000) {
            nilai = tebilang(x/1000000) + " Juta " + tebilang(x%1000000);
        }
        return nilai;
    }

}


